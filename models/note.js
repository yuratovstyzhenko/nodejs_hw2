const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = mongoose.model('note', new Schema({
    userId: {
        required: true,
        type: String,
    },
    completed: {
        type: Boolean,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    createdDate: { 
        type: Date, 
        required: true
    },
}, {
    versionKey: false
}));