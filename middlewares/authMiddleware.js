const jwt = require('jsonwebtoken');

const { secret } = require('../config/auth');

module.exports = (request, response, next) => {
    try{
        const authHeader = request.headers['authorization'];

        if(!authHeader) {
            return response.status(400).json({message: 'No authorization header found'});
        }
        const [, jwtToken] = authHeader.split(' ');
    
        try {
            request.user = jwt.verify(jwtToken, secret);
            next();
        } catch (err) {
            return response.status(401).json({message: 'Unauthorized user. Invalid JWT'});
        }
    } catch (err) {
        return response.status(500).json({message: err.message});
    }
};