const User = require('../models/user');

module.exports.getUser = (request, response) => {
    const { _id } = request.user;

    User.findOne({_id}).exec()
        .then(user => {
            if (!user ) {
                return response.status(401).json({message: 'Unknown user. Register or login first !'});
            }
            response.status(200).json({
                user: {
                    _id: user._id,
                    username: user.username,
                    createdDate: user.createdDate
                }
            });
        })
        .catch(err => {
            response.status(500).json({message: err.message});
        });
};

module.exports.deleteUser = (request, response) => {
    const { _id } = request.user;

    User.deleteOne({_id}).exec()
        .then(user => {
            if (!user ) {
                return response.status(401).json({message: 'Unknown user. Register or login first !'});
            }
            response.status(200).json({message: 'Success'});
        })
        .catch(err => {
            response.status(500).json({message: err.message});
        });
};

module.exports.patchUser = (request, response) => {
    const { _id } = request.user;
    const { oldPassword, newPassword } = request.body;
    if(!oldPassword || !newPassword) {
        return response.status(400).json({message: 'Input oldPassword and newPassword'});
    }
    User.findOne({_id }).exec()
        .then(user => {
            if (!user ) {
                return response.status(401).json({message: 'Unknown user. Register or login first !'});
            }
            if(user.password !== oldPassword) {
                return response.status(403).json({message: 'Invalid password'});
            }
            User.findByIdAndUpdate({_id}, {password : newPassword}).exec()
                .then(() => {
                    response.status(200).json({message: 'Success'});
                })
                .catch((err) => {
                    response.status(500).json({message: err.message});
                });
        })
        .catch(err => {
            response.status(500).json({message: err.message});
        });
};