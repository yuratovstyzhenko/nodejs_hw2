const Note = require('../models/note');

module.exports.addNote = (request, response) => {
    const { text } = request.body;
    const _id = request.user._id;
    if(!text || text === '') {
        return response.status(400).json({message: 'Text field is missing or empty'});
    }
    const note = new Note({ 
        userId: _id, 
        completed: false, 
        text, 
        createdDate: Date.now()
    });
    note.save()
        .then(() => {
            response.status(200).json({message: 'Success'});
        })
        .catch(err => {
            response.status(500).json({message: err.message});
        });
};

module.exports.updateNoteById = (request, response) => {
    const { text } = request.body;
    const { id: noteId } = request.params;
    const { _id: userId } = request.user;
    if(noteId) {
        if(!text || text === '') {
            return response.status(400).json({message: 'Text field is missing or empty'});
        }
        Note.findOne({_id: noteId, userId}).exec()
            .then(note => {
                if (!note ) {
                    return response.status(400).json({message: 'There is no note by current id'});
                }
                Note.findByIdAndUpdate({'_id': noteId, userId}, {text})
                    .then(() => {
                        response.status(200).json({message: 'Success'});
                    })
                    .catch(err => {
                        response.status(500).json({message: err.message});
                    });
                })
            .catch(err => {
                response.status(500).json({ message: err.message });
            });
    } else {
        response.status(400).json({message: 'Note Id is mandatory'});
    }
};

module.exports.checkNoteById = (request, response) => {
    const { id: noteId } = request.params;
    const { _id: userId } = request.user;
    if(noteId) {
        Note.findOne({_id: noteId, userId}).exec()
            .then(note => {
                if (!note ) {
                    return response.status(400).json({message: 'There is no note by current id'});
                }
                Note.findByIdAndUpdate({'_id': noteId, userId}, { completed: !note.completed})
                    .then(() => {
                        response.status(200).json({message: 'Success'});
                    })
                    .catch(err => {
                        response.status(500).json({message: err.message});
                    });
                })
            .catch(err => {
                response.status(500).json({ message: err.message });
            });
    } else {
        response.status(400).json({message: 'Note Id is mandatory'});
    }
};

module.exports.deleteNoteById = (request, response) => {
    const { id: noteId } = request.params;
    const { _id: userId } = request.user;
    if(noteId) {
        Note.findOne({_id: noteId, userId}).exec()
            .then(note => {
                if (!note ) {
                    return response.status(400).json({message: 'There is no note by current id'});
                }
                Note.findByIdAndDelete({'_id': noteId, userId})
                    .then(() => {
                        response.status(200).json({message: 'Success'});
                    })
                    .catch(err => {
                        response.status(500).json({message: err.message});
                    });
                })
            .catch(err => {
                response.status(500).json({ message: err.message });
            });
    } else {
        response.status(400).json({message: 'Note Id is mandatory'});
    }
};

module.exports.getNoteById = (request, response, next) => {
    const { id: noteId } = request.params;
    const { _id: userId } = request.user;
    if(noteId) {
        Note.findOne({_id: noteId, userId}).exec()
        .then(note => {
            if (!note ) {
                return response.status(400).json({message: 'There is no note by current id'});
            }
            response.status(200).json({ note });
        })
        .catch(err => {
            response.status(500).json({ message: err.message });
        });
    } else {
        next();
    }
};

module.exports.getNotes = (request, response) => {
    Note.find({userId: request.user._id}).exec()
        .then(notes => {
            if(notes.length == 0) {
                return response.status(200).json( {message: 'Note list is empty'} );
            }
            response.status(200).json({ notes });
        })
        .catch(err => {
            response.status(500).json({message: err.message});
        });
};

