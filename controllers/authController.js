const jwt = require('jsonwebtoken');
const User = require('../models/user');

const { secret } = require('../config/auth');

module.exports.register = (request, response) => {
    const { username, password } = request.body;
    if(!username || !password) {
        return response.status(400).json({message: 'Input login and password'});
    }
    User.findOne({ username }).exec()
        .then(user => {
            if ( user ) {
                return response.status(403).json({message: 'Current username already exists'});
            }
            const newUser = new User({username, password});
            newUser.save()
                .then(() => {
                    response.json({message: 'Success'});
                })
                .catch(err => {
                    response.status(500).json({message: err.message});
                });
        })
        .catch(err => {
            response.status(500).json({message: err.message});
        });
};

module.exports.login = (request, response) => {
    const { username, password } = request.body;
    if(!username || !password) {
        return response.status(400).json({message: 'Input login and password'});
    }
    User.findOne({username, password}).exec()
        .then(user => {
            if (!user ) {
                return response.status(401).json({message: 'No user with such username and password found'});
            }
            response.json({jwt_token: jwt.sign(JSON.stringify(user), secret)});
        })
        .catch(err => {
            response.status(500).json({message: err.message});
        });
};