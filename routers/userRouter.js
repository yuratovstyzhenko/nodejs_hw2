const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');

const { getUser, deleteUser, patchUser } = require('../controllers/userController');

router.get('/users/me', authMiddleware, getUser);
router.delete('/users/me', authMiddleware, deleteUser);
router.patch('/users/me', authMiddleware, patchUser);

module.exports = router;