const express = require('express');
const router = express.Router();

const { addNote, getNotes, getNoteById, updateNoteById, checkNoteById, deleteNoteById } = require('../controllers/noteController');

const authMiddleware = require('../middlewares/authMiddleware');


router.post('/notes', authMiddleware, addNote);
router.put('/notes/:id', authMiddleware, updateNoteById);
router.patch('/notes/:id', authMiddleware, checkNoteById);
router.delete('/notes/:id', authMiddleware, deleteNoteById);
router.get('/notes/:id', authMiddleware, getNoteById);
router.get('/notes', authMiddleware, getNotes);

module.exports = router;