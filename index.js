const express = require('express');
const mongoose = require('mongoose');
const app = express();
const cors = require('cors');

const { port } = require('./config/server');
const { databaseName, databaseUserName, databasePassword } = require('./config/database');

const userRouter = require('./routers/userRouter');
const authRouter = require('./routers/authRouter');
const noteRouter = require('./routers/noteRouter');


const uri = `mongodb+srv://${databaseUserName}:${databasePassword}@nodejs-hw2.onosi.mongodb.net/${databaseName}?retryWrites=true&w=majority`;

mongoose.connect(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
}).then(() => {
    console.log('Connected to database');
  })
  .catch((err) => {
    console.error(`Error connecting to the database. \n${err}`);
  });

app.use(cors());
app.use(express.json());

app.use('/api', userRouter);
app.use('/api', authRouter);
app.use('/api', noteRouter);

app.listen(port, () => {
    console.log(`Server listens on ${port} port`);
});